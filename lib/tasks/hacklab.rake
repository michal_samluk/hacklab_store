namespace :hacklab do

  desc "Delete all"
  task delete_all: :environment do
    User.delete_all
    Account.delete_all
    LineItem.delete_all
    Item.delete_all
    Order.delete_all
  end

  desc "Creating users with account"
  task create_users_with_account: :environment do
    200.times do
      time = rand((Time.now - 10.days)..Time.now)
      params = {
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          credits: Faker::Commerce.price * 10,
          street_address: Faker::Address.street_address,
          city: Faker::Address.city,
          created_at: time,
          updated_at: time
      }
      user = User.new(email: Faker::Internet.email, created_at: time, updated_at: time)
      user.save!
      account = user.build_account params
      account.save!
    end
  end

  desc "Creating items"
  task create_items: :environment do
    20.times do

      params = {
          price: Faker::Commerce.price,
          name: Faker::Commerce.product_name,
          description: Faker::Lorem.paragraph
      }
      item = Item.new(params)
      item.save!
    end
  end

  desc "Creating orders"
  task create_orders: :environment do

    Account.find_each(batch_size: 10) do |account|
      10.times do
        order = account.orders.build
        order.save!
        line_items = Item.order("RANDOM()").limit(5).map do |item|
          params = {
              item: item,
              order: order,
              quantity: Faker::Number.number(1)
          }
          LineItem.new(params)
        end
        order.line_items = line_items
      end
    end

  end

end