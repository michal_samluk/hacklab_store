class LineItem < ActiveRecord::Base

  belongs_to :item
  belongs_to :order

  validates :item, :order, presence: true
  validates :quantity, numericality: {only_integer: {greater_than: 0}}

end