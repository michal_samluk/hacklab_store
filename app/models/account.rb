class Account < ActiveRecord::Base

  belongs_to :user
  has_many :orders, dependent: :destroy

  validates :user, presence: true
  validates :credits, numeralicity: {greater_than_or_equal_to: 0, lest_than: 1_000_000}

  scope :with_less_than_400_credits, -> { where('credits < ?', 400).order('first_name, last_name') }

end 