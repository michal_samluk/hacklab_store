class Item < ActiveRecord::Base

  has_many :line_items, dependent: :destroy
  has_many :orders, through: :line_items

  validates :name, presence: true
  validates :description, length: {maximum: 3000}, allow_blank: true

  validates :price, numericality: {greater_than: 0}

  scope :ordered, -> { joins(:line_items) }
  scope :unordered, -> { includes(:line_items).where(line_items: {item_id: nil}) }

end