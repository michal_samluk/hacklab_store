class Order < ActiveRecord::Base

  belongs_to :account
  has_many :line_items, dependent: :destroy
  has_many :items, through: :line_items

  validates :account, presence: true

end