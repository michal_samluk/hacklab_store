class User < ActiveRecord::Base

  has_one :account, dependent: :destroy

  validates :email, presence: true, format: /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ # check if at least one @ exists. Such regex is used in devise
  # it does not check if email is valid!

  scope :credits_less_than, ->(ammount) { joins(:account).where('accounts.credits < ?', ammount) }
  scope :recent_users, -> { where('users.created_at > ?', '06-08-2014'.to_date) }

  scope :grouped_by_day_counts, -> { select('count(*), date(users.created_at) as day').group('date(users.created_at)') }

end