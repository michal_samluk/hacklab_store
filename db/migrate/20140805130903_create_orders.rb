class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :account_id

      t.timestamps
    end

    add_index :orders, :account_id
  end
end
