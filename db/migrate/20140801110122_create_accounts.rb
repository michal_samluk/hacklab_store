class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user 
      t.decimal :credits, precision: 8, scale: 2, default: 0
      t.string :street_address 
      t.string :city 
      t.string :first_name 
      t.string :last_name 

      t.timestamps 
    end

    add_index :accounts, :user_id
  end
end
